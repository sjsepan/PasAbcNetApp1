﻿uses MainView;
uses System;

var
    //default to fail code
    returnValue: Int32 := -1;
begin
    try
      System.Windows.Forms.Application.EnableVisualStyles();
      System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
      System.Windows.Forms.Application.Run(new MainForm);
      
      returnValue := 0;
    except
        on Ex: Exception do
        begin
            Console.Error.WriteLine('main:' + Ex.Message);
        end;
    end;
    
    Halt(returnValue);
end.
