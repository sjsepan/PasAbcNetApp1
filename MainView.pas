﻿Unit MainView;

interface

uses System, System.Drawing, System.Windows.Forms;

type
  MainForm = class(Form)
    procedure cmdRun_Click(sender: Object; e: EventArgs);
    procedure menuFileNew_Click(sender: Object; e: EventArgs);
    procedure menuFileOpen_Click(sender: Object; e: EventArgs);
    procedure menuFileSave_Click(sender: Object; e: EventArgs);
    procedure menuFileSaveAs_Click(sender: Object; e: EventArgs);
    procedure menuFilePrint_Click(sender: Object; e: EventArgs);
    procedure menuFileExit_Click(sender: Object; e: EventArgs);
    procedure menuEditUndo_Click(sender: Object; e: EventArgs);
    procedure menuEditRedo_Click(sender: Object; e: EventArgs);
    procedure menuEditSelectAll_Click(sender: Object; e: EventArgs);
    procedure menuEditCut_Click(sender: Object; e: EventArgs);
    procedure menuEditCopy_Click(sender: Object; e: EventArgs);
    procedure menuEditPasteExecute(sender: Object; e: EventArgs);
    procedure menuEditDelete_Click(sender: Object; e: EventArgs);
    procedure menuEditFind_Click(sender: Object; e: EventArgs);
    procedure menuEditReplace_Click(sender: Object; e: EventArgs);
    procedure menuEditRefresh_Click(sender: Object; e: EventArgs);
    procedure menuEditProperties_Click(sender: Object; e: EventArgs);
    procedure menuEditPreferences_Click(sender: Object; e: EventArgs);
    procedure menuMenuNewWindow_Click(sender: Object; e: EventArgs);
    procedure menuMenuTile_Click(sender: Object; e: EventArgs);
    procedure menuMenuCascade_Click(sender: Object; e: EventArgs);
    procedure menuMenuArrangeAllExecute(sender: Object; e: EventArgs);
    procedure menuMenuHideExecute(sender: Object; e: EventArgs);
    procedure menuMenuShowExecute(sender: Object; e: EventArgs);
    procedure menuHelpContents_Click(sender: Object; e: EventArgs);
    procedure menuHelpIndex_Click(sender: Object; e: EventArgs);
    procedure menuHelpOnlineHelp_Click(sender: Object; e: EventArgs);
    procedure menuHelpLicenceInformation_Click(sender: Object; e: EventArgs);
    procedure menuHelpCheckForUpdates_Click(sender: Object; e: EventArgs);
    procedure menuHelpAbout_Click(sender: Object; e: EventArgs);
    procedure MainForm_Load(sender: Object; e: EventArgs);
    procedure MainForm_FormClosing(sender: Object; e: FormClosingEventArgs);
    procedure cmdFont_Click(sender: Object; e: EventArgs);
    procedure cmdColor_Click(sender: Object; e: EventArgs);
  {$region FormDesigner}
  internal
    {$resource MainView.MainForm.resources}
    menuStrip1: MenuStrip;
    menuFile: ToolStripMenuItem;
    menuFileNew: ToolStripMenuItem;
    menuFileOpen: ToolStripMenuItem;
    menuFileSave: ToolStripMenuItem;
    menuFileSaveAs: ToolStripMenuItem;
    toolStripSeparator1: ToolStripSeparator;
    menuFilePrint: ToolStripMenuItem;
    menuFileSeparator2: ToolStripSeparator;
    menuFileExit: ToolStripMenuItem;
    menuEdit: ToolStripMenuItem;
    menuEditUndo: ToolStripMenuItem;
    menuEditRedo: ToolStripMenuItem;
    toolStripSeparator4: ToolStripSeparator;
    menuEditSelectAll: ToolStripMenuItem;
    menuEditCut: ToolStripMenuItem;
    menuEditCopy: ToolStripMenuItem;
    menuEditPaste: ToolStripMenuItem;
    menuEditDelete: ToolStripMenuItem;
    toolStripSeparator5: ToolStripSeparator;
    menuEditFind: ToolStripMenuItem;
    menuEditReplace: ToolStripMenuItem;
    toolStripSeparator6: ToolStripSeparator;
    menuEditRefresh: ToolStripMenuItem;
    menuEditSeparator0: ToolStripSeparator;
    menuEditPreferences: ToolStripMenuItem;
    menuEditProperties: ToolStripMenuItem;
    menuHelp: ToolStripMenuItem;
    menuHelpContents: ToolStripMenuItem;
    menuHelpIndex: ToolStripMenuItem;
    menuHelpOnlineHelp: ToolStripMenuItem;
    toolStripSeparator3: ToolStripSeparator;
    menuHelpLicenceInformation: ToolStripMenuItem;
    menuHelpCheckForUpdates: ToolStripMenuItem;
    toolStripSeparator2: ToolStripSeparator;
    menuHelpAbout: ToolStripMenuItem;
    toolbar: ToolStrip;
    buttonFileNew: ToolStripButton;
    buttonFileOpen: ToolStripButton;
    buttonFileSave: ToolStripButton;
    buttonFilePrint: ToolStripButton;
    buttonSeparator0: ToolStripSeparator;
    buttonEditUndo: ToolStripButton;
    buttonEditRedo: ToolStripButton;
    buttonEditCut: ToolStripButton;
    buttonEditCopy: ToolStripButton;
    buttonEditPaste: ToolStripButton;
    buttonEditDelete: ToolStripButton;
    buttonEditFind: ToolStripButton;
    buttonEditReplace: ToolStripButton;
    buttonEditRefresh: ToolStripButton;
    buttonEditPreferences: ToolStripButton;
    buttonEditProperties: ToolStripButton;
    toolStripSeparator7: ToolStripSeparator;
    statusBar: StatusStrip;
    statusMessage: ToolStripStatusLabel;
    errorMessage: ToolStripStatusLabel;
    progressBar: ToolStripProgressBar;
    iconAction: ToolStripStatusLabel;
    iconDirty: ToolStripStatusLabel;
    chkStillAnotherBoolean: CheckBox;
    txtStillAnotherString: TextBox;
    txtStillAnotherInt: TextBox;
    lblStillAnotherString: &Label;
    lblStillAnotherInt: &Label;
    chkSomeOtherBoolean: CheckBox;
    txtSomeOtherString: TextBox;
    txtSomeOtherInt: TextBox;
    lblSomeOtherString: &Label;
    lblSomeOtherInt: &Label;
    cmdRun: Button;
    chkSomeBoolean: CheckBox;
    txtSomeString: TextBox;
    txtSomeInt: TextBox;
    lblSomeString: &Label;
    lblSomeInt: &Label;
    menuWindow: ToolStripMenuItem;
    menuWindowNewWindow: ToolStripMenuItem;
    menuWindowTile: ToolStripMenuItem;
    menuWindowCascade: ToolStripMenuItem;
    menuWindowArrangeAll: ToolStripMenuItem;
    toolStripSeparator8: ToolStripSeparator;
    menuWindowHide: ToolStripMenuItem;
    menuWindowShow: ToolStripMenuItem;
    cmdFont: Button;
    cmdColor: Button;
    buttonHelpContents: ToolStripButton;
    {$include MainView.MainForm.inc}
  {$endregion FormDesigner}

    //from here down we should be able to edit directly
    procedure delayFor(dt: double);
    procedure DoSomethingElse();
    function Something(): Boolean;
    procedure FileOpenAction();
    procedure FileSaveAction();
    procedure FilePrintAction();
    procedure EditPreferencesAction();
    procedure HelpAboutAction();
    procedure FontAction();
    procedure ColorAction();
    Procedure StartProgressBarWithPicture
    (
              sStatusMessage, sErrorMessage : String;
              isMarqueeProgressBarStyle, isCountProgressbar:Boolean;
              iProgressBarValue, iProgressBarMax:LongInt;
              var ctlStatusMessage, ctlErrorMessage: ToolStripStatusLabel;
              var ctlProgressBar:ToolStripProgressBar;
              var ctlActionIcon:ToolStripStatusLabel;
               objImage:Image
    );
    procedure UpdateProgressBar
    (
              sStatusMessage : String;
              isMarqueeProgressBarStyle, isCountProgressbar:Boolean;
              iProgressBarValue:LongInt;
              var ctlStatusMessage: ToolStripStatusLabel;
              var ctlProgressBar:ToolStripProgressBar
    );
    procedure UpdateStatusBarMessages
    (
              sStatusMessage, sErrorMessage : String;
              var ctlStatusMessage, ctlErrorMessage: ToolStripStatusLabel
    );
    procedure StopProgressBar
    (
              sStatusMessage, sErrorMessage : String;
              var ctlStatusMessage, ctlErrorMessage: ToolStripStatusLabel;
              var ctlProgressBar:ToolStripProgressBar;
              var ctlActionIcon:ToolStripStatusLabel
    );

  public
    constructor;
    begin
      InitializeComponent;
    end;
  end;//type


    const
        APP_TITLE: String = 'PasAbcNetApp1';
        APP_TITLE_FORMAT: String = '''{0}'' - ' + APP_TITLE;//PasAbcNetApp1';
        ACTION_IN_PROGRESS: String = ' ...';
        ACTION_CANCELLED: String = ' cancelled';
        ACTION_DONE: String = ' done';
    
    var
        bStopControlEvents: Boolean;

implementation

{$region Utility}
procedure MainForm.delayFor(dt: double);
var
    tc: double;
begin
    tc := Environment.TickCount;{GetTickCount64;}
    repeat
        application.DoEvents{ProcessMessages};
    until (Environment.TickCount{GetTickCount64} > tc + dt){or (Application.Terminated)};
end;

/// <summary>
/// modify fields
/// </summary>
procedure MainForm.DoSomethingElse();
begin
    self.chkSomeBoolean.Checked := not self.chkSomeBoolean.Checked;
    self.txtSomeInt.Text := (Int32.Parse(self.txtSomeInt.Text) + 1).ToString();//do integer conversion, not string
    self.txtSomeString.Text := DateTime.Now.ToString();
    
    self.chkSomeOtherBoolean.Checked := not self.chkSomeOtherBoolean.Checked;
    self.txtSomeOtherInt.Text := (Int32.Parse(self.txtSomeOtherInt.Text) + 1).ToString();//Note:PascalAbc.Net is extended with += etc., but these conversions need to happen in-beteeen in this case.
    self.txtSomeOtherString.Text := DateTime.Now.ToString();
    
    self.chkStillAnotherBoolean.Checked := not self.chkStillAnotherBoolean.Checked;
    self.txtStillAnotherInt.Text := (Int32.Parse(self.txtStillAnotherInt.Text) + 1).ToString();
    self.txtStillAnotherString.Text := DateTime.Now.ToString();
end;


function MainForm.Something(): Boolean;
begin
    Application.DoEvents{ProcessMessages};
    delayFor(3000);
    Something := True;
end;

{$endregion Utility}

{$region Actions}

procedure MainForm.cmdRun_Click(sender: Object; e: EventArgs);
var
    sStatusMessage, sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Run' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil{self.cmdRun.Image});
            
            //perform sender disable/enable in all actions
            self.cmdRun.Enabled := False;
        
            DoSomethingElse();
            sStatusMessage := 'Run' + ACTION_DONE;
    
        finally
            //always do this
            self.cmdRun.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;//
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('cmdRun_Click:' + Ex.Message);
        end;
    end;       
end;

procedure MainForm.cmdFont_Click(sender: Object; e: EventArgs);
var
    sStatusMessage, sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Font' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil{self.cmdFont.Image});
            
            //perform sender disable/enable in all actions
            self.cmdFont.Enabled := False;
        
            FontAction();
            sStatusMessage := self.statusMessage.Text + ACTION_DONE;
        finally
            //always do this
            self.cmdFont.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;//
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('cmdFont_Click:' + Ex.Message);
        end;
    end;       
end;

procedure MainForm.cmdColor_Click(sender: Object; e: EventArgs);
var
    sStatusMessage, sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Color' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil{self.cmdColor.Image});
            
            //perform sender disable/enable in all actions
            self.cmdColor.Enabled := False;
        
            ColorAction();
            sStatusMessage := self.statusMessage.Text + ACTION_DONE;
        finally
            //always do this
            self.cmdColor.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;//
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('cmdColor_Click:' + Ex.Message);
        end;
    end;       
end;

procedure MainForm.MainForm_Load(sender: Object; e: EventArgs);
begin
  
end;

procedure MainForm.MainForm_FormClosing(sender: Object; e: FormClosingEventArgs);
var
    sStatusMessage, {sStatusMessageFromCheck,} sErrorMessage: String;
    bCancel: Boolean;
begin
    try
        try
            //sStatusMessageFromCheck := '';
            //bCancel := CheckForSaveOrCancel(sStatusMessageFromCheck);
            
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Exit' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuFileExit.Enabled := False;
            //self.buttonFileExit.Enabled := False;
            
            //PROMPT
            //TODO:dialog
              Case MessageBox.Show('Quit?', APP_TITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question) of
                   System.Windows.Forms.DialogResult.Yes:
                   begin
                      //Yes, QUIT
                      bCancel := False;
                   end;
                   System.Windows.Forms.DialogResult.No:
                   begin
                      //No, skip Save-or-cancel, do target action
                      bCancel := True;
                   end;
                   Else
                      raise Exception.Create('unexpected response enum');
              End; //case
            
            if bCancel Then
            begin
                e.Cancel := true;
                sStatusMessage := sStatusMessage + ACTION_CANCELLED;
            end
            Else
            begin
                //EXIT
                //Self.Close() or window close button already triggered; simply leave Cancel default as false
            end;
        
        finally
            //always do this (exccept here when exiting)
            if bCancel Then
            begin
                self.menuFileExit.Enabled := True;
                //self.buttonFileExit.Enabled := True;
                StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
            end;
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuFileExit_Click:' + Ex.Message);
        end;
    end;
end;

{$region File}

procedure MainForm.menuFileNew_Click(sender: Object; e: EventArgs);
var
    sStatusMessage, sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'New' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuFileNew.Image);
            
            //perform sender disable/enable in all actions
            self.menuFileNew.Enabled := False;
            self.buttonFileNew.Enabled := False;
        
            if Something() Then //TODO:New
            begin
                sStatusMessage := 'New' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'New' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuFileNew.Enabled := True;
            self.buttonFileNew.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuFileNew_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuFileOpen_Click(sender: Object; e: EventArgs);
var
    sStatusMessage, sErrorMessage: String;
   //bCancel : Boolean;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Open' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuFileOpen.Image);//
            
            //perform sender disable/enable in all actions
            self.menuFileOpen.Enabled := False;
            self.buttonFileOpen.Enabled := False;
        
            //OPEN
            FileOpenAction();
            sStatusMessage := self.statusMessage.Text + ACTION_DONE;
        finally
            //always do this
            self.menuFileOpen.Enabled := True;
            self.buttonFileOpen.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuFileOpen_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuFileSave_Click(sender: Object; e: EventArgs);
var
    sStatusMessage, sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Save' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuFileSave.Image);//
            
            //perform sender disable/enable in all actions
            self.menuFileSave.Enabled := False;
            self.buttonFileSave.Enabled := False;
        
            //SAVE
            FileSaveAction();
            sStatusMessage := self.statusMessage.Text + ACTION_DONE;
        finally
            //always do this
            self.menuFileSave.Enabled := True;
            self.buttonFileSave.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuFileSave_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuFileSaveAs_Click(sender: Object; e: EventArgs);
var
    sStatusMessage, sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Save As' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuFileSaveAs.Enabled := False;
            //self.buttonFileSaveAs.Enabled := False;
        
            //SAVE AS
            FileSaveAction();
            sStatusMessage := self.statusMessage.Text + ACTION_DONE;
        finally
            //always do this
            self.menuFileSaveAs.Enabled := True;
            //self.buttonFileSaveAs.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuFileSaveAs_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuFilePrint_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Print' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuFilePrint.Image);//
            
            //perform sender disable/enable in all actions
            self.menuFilePrint.Enabled := False;
            self.buttonFilePrint.Enabled := False;
            
            //PRINT
            FilePrintAction();
            sStatusMessage := self.statusMessage.Text + ACTION_DONE;
        finally
            //always do this
            self.menuFilePrint.Enabled := True;
            self.buttonFilePrint.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuFilePrint_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuFileExit_Click(sender: Object; e: EventArgs);//TODO:just call Self.Close(); let formclosing do check and cancel if needed
begin
    Self.Close();
end;
{$endregion File}

{$region Edit}
procedure MainForm.menuEditUndo_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Undo...';
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditUndo.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditUndo.Enabled := False;
            self.buttonEditUndo.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Undo' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Undo' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditUndo.Enabled := True;
            self.buttonEditUndo.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditUndo_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditRedo_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Redo' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditRedo.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditRedo.Enabled := False;
            self.buttonEditRedo.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Redo' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Redo' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditRedo.Enabled := True;
            self.buttonEditRedo.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditRedo_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditSelectAll_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Select All' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuEditSelectAll.Enabled := False;
            //self.buttonEditSelectAll.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Select All' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Select All' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditSelectAll.Enabled := True;
            //self.buttonEditSelectAll.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditSelectAll_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditCut_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Cut' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditCut.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditCut.Enabled := False;
            self.buttonEditCut.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Cut' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Cut' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditCut.Enabled := True;
            self.buttonEditCut.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditCut_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditCopy_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Copy' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditCopy.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditCopy.Enabled := False;
            self.buttonEditCopy.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Copy' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Copy' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditCopy.Enabled := True;
            self.buttonEditCopy.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditCopy_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditPasteExecute(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Paste' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditPaste.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditPaste.Enabled := False;
            self.buttonEditPaste.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Paste' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Paste' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditPaste.Enabled := True;
            self.buttonEditPaste.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditPasteExecute:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditDelete_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Delete' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditDelete.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditDelete.Enabled := False;
            //self.buttonEditDelete.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Delete' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Delete' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditDelete.Enabled := True;
            //self.buttonEditDelete.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditDelete_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditFind_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Find' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditFind.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditFind.Enabled := False;
            self.buttonEditFind.Enabled := False;
            
            if Something() Then //TODO:TFindDialog
            begin
                sStatusMessage := 'Find' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Find' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditFind.Enabled := True;
            self.buttonEditFind.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditFind_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditReplace_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Replace' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditReplace.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditReplace.Enabled := False;
            self.buttonEditReplace.Enabled := False;
            
            if Something() Then    //TODO:TReplaceDialog
            begin
                sStatusMessage := 'Replace' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Replace' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditReplace.Enabled := True;
            self.buttonEditReplace.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditReplace_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditRefresh_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Refresh...';
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditRefresh.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditRefresh.Enabled := False;
            self.buttonEditRefresh.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Refresh' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Refresh' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditRefresh.Enabled := True;
            self.buttonEditRefresh.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditRefresh_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditPreferences_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Preferences' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditPreferences.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditPreferences.Enabled := False;
            self.buttonEditPreferences.Enabled := False;
            
            //PREFERENCES
            EditPreferencesAction();
            sStatusMessage := self.statusMessage.Text + ACTION_DONE;
        finally
            //always do this
            self.menuEditPreferences.Enabled := True;
            self.buttonEditPreferences.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditPreferences_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuEditProperties_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Properties' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuEditProperties.Image);//
            
            //perform sender disable/enable in all actions
            self.menuEditProperties.Enabled := False;
            self.buttonEditProperties.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Properties' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Properties' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuEditProperties.Enabled := True;
            self.buttonEditProperties.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuEditProperties_Click:' + Ex.Message);
        end;
    end;
end;

{$endregion Edit}

{$region Window}
procedure MainForm.menuMenuNewWindow_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'New Window' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuWindowNewWindow.Enabled := False;
            //self.buttonWindowNewWindow.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'New Window' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'New Window' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuWindowNewWindow.Enabled := True;
            //self.buttonWindowNewWindow.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuMenuNewWindow_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuMenuTile_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Tile' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuWindowTile.Enabled := False;
            //self.buttonWindowTile.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Tile' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Tile' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuWindowTile.Enabled := True;
            //self.buttonWindowTile.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuMenuTile_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuMenuCascade_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Cascade' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuWindowCascade.Enabled := False;
            //self.buttonWindowCascade.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Cascade' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Cascade' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuWindowCascade.Enabled := True;
            //self.buttonWindowCascade.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuMenuCascade_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuMenuArrangeAllExecute(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Arrange All' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuWindowArrangeAll.Enabled := False;
            //self.buttonWindowArrangeAll.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Arrange All' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Arrange All' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuWindowArrangeAll.Enabled := True;
            //self.buttonWindowArrangeAll.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuMenuArrangeAllExecute:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuMenuHideExecute(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Hide' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuWindowHide.Enabled := False;
            //self.buttonWindowHide.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Hide' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Hide' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuWindowHide.Enabled := True;
            //self.buttonWindowHide.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuMenuHideExecute:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuMenuShowExecute(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Show' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuWindowShow.Enabled := False;
            //self.buttonWindowShow.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Show' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Show' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuWindowShow.Enabled := True;
            //self.buttonWindowShow.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuMenuShowExecute:' + Ex.Message);
        end;
    end;
end;
{$endregion Window}

{$region Help}
procedure MainForm.menuHelpContents_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Help Contents' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, self.menuHelpContents.Image);//
            
            //perform sender disable/enable in all actions
            self.menuHelpContents.Enabled := False;
            self.buttonHelpContents.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Help Contents' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Help Contents' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuHelpContents.Enabled := True;
            self.buttonHelpContents.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuHelpContents_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuHelpIndex_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Help Index' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuHelpIndex.Enabled := False;
            //self.buttonHelpHelpIndex.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Help Index' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Help Index' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuHelpIndex.Enabled := True;
            //self.buttonHelpHelpIndex.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuHelpIndex_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuHelpOnlineHelp_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Online Help' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuHelpOnlineHelp.Enabled := False;
            //self.buttonHelpHelpOnHelp.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Online Help' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Online Help' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuHelpOnlineHelp.Enabled := True;
            //self.buttonHelpHelpOnHelp.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuHelpOnlineHelp_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuHelpLicenceInformation_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Licence Information' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuHelpLicenceInformation.Enabled := False;
            //self.buttonHelpLicenceInformation.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Licence Information' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Licence Information' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuHelpLicenceInformation.Enabled := True;
            //self.buttonHelpLicenceInformation.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuHelpLicenceInformation_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuHelpCheckForUpdates_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'Check For Updates' + ACTION_IN_PROGRESS;
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage, sErrorMessage, True, False, 0, 100, statusMessage, errorMessage, progressBar, iconAction, nil);//
            
            //perform sender disable/enable in all actions
            self.menuHelpCheckForUpdates.Enabled := False;
            //self.buttonHelpCheckForUpdates.Enabled := False;
            
            if Something() Then
            begin
                sStatusMessage := 'Check For Updates' + ACTION_DONE;
            end
            Else
            begin
                sStatusMessage := 'Check For Updates' + ACTION_CANCELLED;
            end;
        finally
            //always do this
            self.menuHelpCheckForUpdates.Enabled := True;
            //self.buttonHelpCheckForUpdates.Enabled := True;
            StopProgressBar(sStatusMessage, sErrorMessage, statusMessage, errorMessage, progressBar, iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('', Ex.Message, statusMessage, errorMessage, progressBar, iconAction);
            Console.Error.WriteLine('menuHelpCheckForUpdates_Click:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.menuHelpAbout_Click(sender: Object; e: EventArgs);
var
    sStatusMessage: String;
    sErrorMessage: String;
begin
    try
        try
            //clear status, error messages at beginning of every action
            sStatusMessage := 'About' + ACTION_IN_PROGRESS + 'Generic GUI w/ PascalABC.Net';
            sErrorMessage := '';
            
            //use progress bar (marquee) with action icon (where available) in status bar
            StartProgressBarWithPicture(sStatusMessage,sErrorMessage,True,False,0,100,statusMessage,errorMessage,progressBar,iconAction,self.menuHelpAbout.Image);//
            
            //perform sender disable/enable in all actions
            self.menuHelpAbout.Enabled := False;
            //self.buttonHelpAbout.Enabled := False;
            
            //ABOUT
            HelpAboutAction();
            sStatusMessage := self.statusMessage.Text + ACTION_DONE;
        finally
            //always do this
            self.menuHelpAbout.Enabled := True;
            //self.buttonHelpAbout.Enabled := True;
            StopProgressBar(sStatusMessage,sErrorMessage,statusMessage,errorMessage,progressBar,iconAction);
        end;
    except
        on Ex: Exception do
        begin
            StopProgressBar('',Ex.Message,statusMessage,errorMessage,progressBar,iconAction);
            Console.Error.WriteLine('menuHelpAbout_Click:' + Ex.Message);
        end;
    end;
end;
{$endregion Help}

procedure MainForm.FileOpenAction();
var
    openFileDialog: OpenFileDialog;
    response: System.Windows.Forms.DialogResult;
begin
    try
        response := System.Windows.Forms.DialogResult.None;
    
        openFileDialog := new System.Windows.Forms.OpenFileDialog;
        openFileDialog.Multiselect := false;

        response := openFileDialog.ShowDialog(self); 
        
        if (response = System.Windows.Forms.DialogResult.OK) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + openFileDialog.FileName + ACTION_IN_PROGRESS;
        end
        else if (response = System.Windows.Forms.DialogResult.Cancel) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + ACTION_CANCELLED + ACTION_IN_PROGRESS;
        end;
    
        openFileDialog.Dispose();
    
    except
        on Ex: Exception do
        begin
            Console.Error.WriteLine('FileOpenAction:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.FileSaveAction();
var
    saveFileDialog: SaveFileDialog;
    response: System.Windows.Forms.DialogResult;
begin
    try
        response := System.Windows.Forms.DialogResult.None;
    
        saveFileDialog := new System.Windows.Forms.SaveFileDialog;

        response := saveFileDialog.ShowDialog(self); 
        
        if (response = System.Windows.Forms.DialogResult.OK) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + saveFileDialog.FileName + ACTION_IN_PROGRESS;
        end
        else if (response = System.Windows.Forms.DialogResult.Cancel) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + ACTION_CANCELLED + ACTION_IN_PROGRESS;
        end;
    
        saveFileDialog.Dispose();
    
    except
        on Ex: Exception do
        begin
            Console.Error.WriteLine('FileSaveAction:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.FilePrintAction();
var
    printDialog: PrintDialog;
    response: System.Windows.Forms.DialogResult;
begin
    try
        response := System.Windows.Forms.DialogResult.None;
    
        printDialog := new System.Windows.Forms.PrintDialog;

        response := printDialog.ShowDialog(self); 
        
        if (response = System.Windows.Forms.DialogResult.OK) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + printDialog.PrinterSettings.PrinterName + ACTION_IN_PROGRESS;
        end
        else if (response = System.Windows.Forms.DialogResult.Cancel) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + ACTION_CANCELLED + ACTION_IN_PROGRESS;
        end;
    
        printDialog.Dispose();
    
    except
        on Ex: Exception do
        begin
            Console.Error.WriteLine('FilePrintAction:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.EditPreferencesAction();
var
    folderBrowserDialog: FolderBrowserDialog;
    response: System.Windows.Forms.DialogResult;
begin
    try
        response := System.Windows.Forms.DialogResult.None;
    
        folderBrowserDialog := new System.Windows.Forms.FolderBrowserDialog;

        response := folderBrowserDialog.ShowDialog(self); 
        
        if (response = System.Windows.Forms.DialogResult.OK) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + folderBrowserDialog.SelectedPath + ACTION_IN_PROGRESS;
        end
        else if (response = System.Windows.Forms.DialogResult.Cancel) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + ACTION_CANCELLED + ACTION_IN_PROGRESS;
        end;
    
        folderBrowserDialog.Dispose();
    
    except
        on Ex: Exception do
        begin
            Console.Error.WriteLine('EditPreferencesAction:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.HelpAboutAction();
var
    response: System.Windows.Forms.DialogResult;
    message: String := nil;
begin
    try
        response := System.Windows.Forms.DialogResult.None;
    
        message += 'ProgramName = ' + APP_TITLE + System.Environment.NewLine;
        message += 'Version = 0.1' + System.Environment.NewLine;
        message += 'Desktop GUI app prototype, on Windows(/Linux), in Pascal / WinForms(/Mono), using PascalABC.Net' + System.Environment.NewLine;
        message += 'WebsiteLabel = GitLab' + System.Environment.NewLine;
        message += 'Website = http://www.gitlab.com' + System.Environment.NewLine;
        // // aboutDialog.Platform;r/o
        message += 'Copyright = Copyright (C) 2022 Stephen J Sepan' + System.Environment.NewLine;
        message += 'Designers = Stephen J Sepan' + System.Environment.NewLine;
        message += 'Developers = Stephen J Sepan' + System.Environment.NewLine;
        message += 'Documenters = Stephen J Sepan' + System.Environment.NewLine;
        message += 'License = License text included w/ source';

        response := MessageBox.Show(Message, APP_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information); 
        
        if (response = System.Windows.Forms.DialogResult.OK) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + ACTION_IN_PROGRESS;
        end;
    
    except
        on Ex: Exception do
        begin
            Console.Error.WriteLine('HelpAboutAction:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.FontAction();
var
    fontDialog: FontDialog;
    response: System.Windows.Forms.DialogResult;
begin
    try
        response := System.Windows.Forms.DialogResult.None;
    
        fontDialog := new System.Windows.Forms.FontDialog;

        response := fontDialog.ShowDialog(self); 
        
        if (response = System.Windows.Forms.DialogResult.OK) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + fontDialog.Font.Name + ACTION_IN_PROGRESS;
        end
        else if (response = System.Windows.Forms.DialogResult.Cancel) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + ACTION_CANCELLED + ACTION_IN_PROGRESS;
        end;
    
        fontDialog.Dispose();
    
    except
        on Ex: Exception do
        begin
            Console.Error.WriteLine('FontAction:' + Ex.Message);
        end;
    end;
end;

procedure MainForm.ColorAction();
var
    colorDialog: ColorDialog;
    response: System.Windows.Forms.DialogResult;
begin
    try
        response := System.Windows.Forms.DialogResult.None;
    
        colorDialog := new System.Windows.Forms.ColorDialog;

        response := colorDialog.ShowDialog(self); 
        
        if (response = System.Windows.Forms.DialogResult.OK) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + colorDialog.Color.Name + ACTION_IN_PROGRESS;
        end
        else if (response = System.Windows.Forms.DialogResult.Cancel) then
        begin
            self.statusMessage.Text := self.statusMessage.Text + ACTION_CANCELLED + ACTION_IN_PROGRESS;
        end;
    
        colorDialog.Dispose();
    
    except
        on Ex: Exception do
        begin
            Console.Error.WriteLine('ColorAction:' + Ex.Message);
        end;
    end;
end;

{$endregion Actions}

// <summary>
// Use for Marquee-style progress bar, or when percentages/count must be indicated.
// </summary>
// <param name="sStatusMessage">String</param>
// <param name="serrorMessage">String</param>
// <param name="isMarqueeProgressBarStyle">Boolean</param>
// <param name="isCountProgressbar">Boolean</param>
// <param name="iProgressBarValue">LongInt</param>
// <param name="iProgressBarMax">LongInt. ignored when using marquee or using normal and not count (i.e. - percentage); defaults to 100</param>
// <param name="ctlStatusMessage">ToolStripStatusLabel</param>
// <param name="ctlErrorMessage">ToolStripStatusLabel</param>
// <param name="ctlProgressBar">ToolStripProgressBar</param>
// <param name="ctlActionIcon">ToolStripStatusLabel</param>
// <param name="objImage">Image</param>
procedure MainForm.StartProgressBarWithPicture
(
  sStatusMessage, sErrorMessage : String;
  isMarqueeProgressBarStyle, isCountProgressbar:Boolean;
  iProgressBarValue, iProgressBarMax:LongInt;
  var ctlStatusMessage, ctlErrorMessage: ToolStripStatusLabel;
  var ctlProgressBar:ToolStripProgressBar;
  var ctlActionIcon:ToolStripStatusLabel;
  objImage:Image
);
begin
  try
    try
       ctlProgressBar.Visible:=True;
       ctlProgressBar.Enabled:=True;

       if (isMarqueeProgressBarStyle) then
       begin
            //marquee
            ctlProgressBar.Style:= ProgressBarStyle.Marquee;
            ctlProgressBar.Maximum:=100;
            ctlProgressBar.Step := 1;
            ctlProgressBar.Value := 33;//one third
       end
       else
       begin
            //progress
            //if Style is not Marquee, then we are marking either a count or percentage
            ctlProgressBar.Style:=ProgressBarStyle.Blocks;
            if (isCountProgressbar) then
            begin
                 //count
                 //set to smooth if count is used.
                 ctlProgressBar.Style := ProgressBarStyle.Continuous;
                 ctlProgressBar.Maximum:=iProgressBarMax;

            end
            else
            begin
                 //percentage
                 //set to blocks if actual percentage is used.
                 ctlProgressBar.Style := ProgressBarStyle.Blocks;
                 ctlProgressBar.Maximum:=100;//

            end;

            //set to value if percentage or count used.
            ctlProgressBar.Step := 1;
            ctlProgressBar.Value := iProgressBarValue;
       end;

      if (sStatusMessage='') then ctlStatusMessage.Text := '' else ctlStatusMessage.Text := sStatusMessage;

      if (sErrorMessage='') then ctlErrorMessage.Text := '' else ctlErrorMessage.Text := sErrorMessage;
      ctlErrorMessage.ToolTipText := ctlErrorMessage.Text;

      if (objImage <> nil) then
      begin
        ctlActionIcon.Image := objImage;
        ctlActionIcon.ToolTipText := sStatusMessage;
        ctlActionIcon.Visible := True;
      end;
    finally
      //give the app time to draw the eye-candy, even if its only for an instant
      Application.DoEvents;//ProcessMessages;
    end;
  except
    on Ex: Exception do
        Console.Error.WriteLine('StartProgressBarWithPicture:' + Ex.Message);
  end;
End; //procedure



// <summary>
// Update percentage changes.
// </summary>
// <param name="sStatusMessage">String. If Null, do nothing, otherwise update.</param>
// <param name="isMarqueeProgressBarStyle">Boolean</param>
// <param name="isCountProgressbar">Boolean</param>
// <param name="iProgressBarValue">LongInt. UpdateProgressBar does not manage the value, other than checking that it is within the range, and adjusting the Max for counting mode.</param>
// <param name="ctlStatusMessage">ToolStripStatusLabel</param>
// <param name="ctlProgressBar">ToolStripProgressBar</param>
procedure MainForm.UpdateProgressBar
(
          sStatusMessage : String;
          isMarqueeProgressBarStyle, isCountProgressbar:Boolean;
          iProgressBarValue:LongInt;
          var ctlStatusMessage: ToolStripStatusLabel;
          var ctlProgressBar:ToolStripProgressBar
);
begin
  try
    try

    if (sStatusMessage='') then {ctlStatusMessage.Text := ''} else ctlStatusMessage.Text := sStatusMessage;

    if (isMarqueeProgressBarStyle) then
     begin
          //do nothing with progressbar
     end
     else
     begin
         //update count or percentage
          //if Style is not Marquee, then we are marking either a count or percentage
          //if we are simply counting, the progress bar will periodically need to adjust the Maximum.
          if (isCountProgressbar) then
          begin
            If (iProgressBarValue > ctlProgressBar.Maximum) Then
            begin
                 ctlProgressBar.Maximum := iProgressBarValue * 2;
            end;
          end;

          ctlProgressBar.Value := iProgressBarValue;
    end;

    finally
      //give the app time to draw the eye-candy, even if its only for an instant
        Application.DoEvents;
    end;
  except
    on Ex: Exception do
        Console.Error.WriteLine('UpdateProgressBar:' + Ex.Message);
  end;
end; //Sub

// <summary>
// Update message(s) only, without changing progress bar.
// Null parameter will leave a message unchanged; '' will clear it.
// </summary>
// <param name="sStatusMessage">String</param>
// <param name="serrorMessage">String</param>
// <param name="ctlStatusMessage">ToolStripStatusLabel</param>
// <param name="ctlErrorMessage">ToolStripStatusLabel</param>
procedure MainForm.UpdateStatusBarMessages
(
          sStatusMessage, sErrorMessage : String;
          var ctlStatusMessage, ctlErrorMessage: ToolStripStatusLabel
);
begin
  try
    try
       if (sStatusMessage='') then {ctlStatusMessage.Text := ''} else ctlStatusMessage.Text := sStatusMessage;

       if (sErrorMessage='') then {ctlErrorMessage.Text := ''} else ctlErrorMessage.Text := sErrorMessage;
       ctlErrorMessage.ToolTipText := ctlErrorMessage.Text;

    finally
      //give the app time to draw the eye-candy, even if its only for an instant
      Application.DoEvents;
    end;

  except
    on Ex: Exception do
        Console.Error.WriteLine('UpdateStatusBarMessages:' + Ex.Message);
  end;

End; //Sub


// <summary>
// Stop progress bar and display messages.
// Application.ProcessMessages will ensure messages are processed before continuing.
// </summary>
// <param name="sStatusMessage">String</param>
// <param name="serrorMessage">String</param>
// <param name="ctlStatusMessage">ToolStripStatusLabel</param>
// <param name="ctlErrorMessage">ToolStripStatusLabel</param>
// <param name="ctlProgressBar">ToolStripProgressBar</param>
// <param name="ctlActionIcon">ToolStripStatusLabel</param>
procedure MainForm.StopProgressBar
(
          sStatusMessage, sErrorMessage : String;
          var ctlStatusMessage, ctlErrorMessage: ToolStripStatusLabel;
          var ctlProgressBar:ToolStripProgressBar;
          var ctlActionIcon:ToolStripStatusLabel
);
begin
    try
       try

          if (sStatusMessage='') then ctlStatusMessage.Text := '' else ctlStatusMessage.Text := sStatusMessage;

          //do not clear error at end of operation, clear it at start of operation
          if (sErrorMessage='') then {do nothing} else ctlErrorMessage.Text := sErrorMessage;
          //sync
          ctlErrorMessage.ToolTipText := ctlErrorMessage.Text;

          ctlProgressBar.Enabled:=False;
          ctlProgressBar.Visible:=False;

          ctlActionIcon.Visible := False;

        finally
           ctlProgressBar.Enabled:=False;
           ctlProgressBar.Visible:=False;

          //give the app time to draw the eye-candy, even if its only for an instant
          Application.DoEvents;
        end;

     except
       on Ex: Exception do
            Console.Error.WriteLine('StopProgressBar:' + Ex.Message);
     end;

End;

end.
