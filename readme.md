# readme for PasAbcNetApp1 v0.1

## About

Desktop GUI app prototype, on Windows(/Linux), in Pascal / WinForms(/Mono), using PascalABC.Net. It and the IDE require .Net Framework 4.0.

![PasAbcNetApp1.PNG](./PasAbcNetApp1.PNG?raw=true "Screenshot")

### Notes

~My first PascalABC.Net project, MvcFormPAbc, was little more than a direct port from FreePascal/Lazarus IDE on Linux. It included some code to read/write XML, putting it halfway between a prototype like this app and a fuller demo app ( like the upcoming project, MvcFormsPasAbcNet). It did not follow some of the design conventions that I've used in the most recent prototype and demo apps, like Close-checking and demonstrating the use of dialogs. The upcoming demo project will address this, after which I may deprecate MvcFormPAbc.
~PascalABC.Net uses their own form of partial-class by writing the designer info in a .inc file.

### VSCode

VSCode was not used for the primary development, which was done in the PascalABC.Net IDE. However, documentation and preparation for posting was done in Code.
To nest files associated with forms, use the setting 'Features/Explorer/File Nesting: Patterns':
Item="*.cs", Value="${basename}.resx,${basename}.Designer.cs"

### Instructions for downloading/installing Mono (Linux)

Locate / install 'mono-complete' from your distribution's package manager.

If you are using Visual Basic, then you will likely also want to install ​mono-vbnc​ for dependencies such as ​Microsoft.VisualBasic​.
sudo apt-get install mono-vbnc

### Run from terminal (Mono/Linux)

Run ('mono PasAbcNetApp1.exe') from PasAbcNetApp1 project folder. PascalABC.Net does not generate bin/Debug sub-folder.

### Issues

~The entry point created by the IDE did not seem to open up the possibility of returning return-codes from a main(), or even to take command-line arguments there. I am sure there must be a way, but I will have to research it and try later. UPDATE:using Halt() with errorlevel parameter.

### History

0.1:
~initial release

Steve Sepan
sjsepan@yahoo.com
10/16/2022
